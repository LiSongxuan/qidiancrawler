# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html


# useful for handling different item types with a single interface
from itemadapter import ItemAdapter
import sqlalchemy as sqla
from sqlalchemy.ext.declarative import declarative_base
import datetime

import re

class BookPipeline:
    Base = declarative_base()
    engine = sqla.create_engine('mysql+pymysql://chong:123456@10.28.208.161/new',encoding= "utf-8", echo=True)

    class novel(Base):
        __tablename__='novel'
        id = sqla.Column(sqla.Integer,primary_key=True,autoincrement=True)
        name = sqla.Column(sqla.String(255))
        imgLink = sqla.Column(sqla.String(255))
        link = sqla.Column(sqla.String(255))
        content1 = sqla.Column(sqla.String(255))
        author = sqla.Column(sqla.String(255))
        tag = sqla.Column(sqla.String(255))
        label = sqla.Column(sqla.String(255))
        wordCount= sqla.Column(sqla.String(255))
        def __init__(self,n,i,l,c,a,t,ll,wc):
            self.name = n
            self.imgLink = i
            self.link=l
            self.content1=c
            self.author=a
            self.tag=t
            self.label=ll
            self.wordCount=wc

    class allrecommend(Base):
        __tablename__ = 'allrecommend'
        id = sqla.Column(sqla.Integer,primary_key=True)
        bookId = sqla.Column(sqla.Integer)
        allRecommend = sqla.Column(sqla.String(255))
        date= sqla.Column(sqla.Date)
        def __init__(self,bi,ar):
            self.bookId=bi
            self.allRecommend=ar
            self.date=datetime.datetime.now()

    class weekrecommend(Base):
        __tablename__ = 'weekrecommend'
        id = sqla.Column(sqla.Integer,primary_key=True)
        bookId = sqla.Column(sqla.Integer)
        weekRecommend = sqla.Column(sqla.String(255))
        weekDate= sqla.Column(sqla.Date)
        def __init__(self,bi,wr):
            self.bookId=bi
            self.weekRecommend=wr
            self.weekDate=datetime.datetime.now()
    def open_spider(self, spider):
        self.Base.metadata.create_all(self.engine)

    def process_item(self, item, spider):
        item["intro"] = self.process_content(item["intro"])
        DBSession = sqla.orm.sessionmaker(bind=self.engine)
        session = DBSession()
        n = self.novel(item['title'],item['img'],item['url'],item['intro'],item['author'],"".join(item['tag']),item['type'],item["wordcount"])

        q=session.query(self.novel).filter(self.novel.link == item['url']).all()
        if len(q)==0:
            session.add(n)
            session.commit()
        q=session.query(self.novel).filter(self.novel.link == item['url']).first()
        ar = self.allrecommend(q.id,item['totalrecommand'])
        wr=self.weekrecommend(q.id,item['weeklyrecommand'])
        session.add(ar)
        session.add(wr)
        session.commit()


        print(item)
        return item

    def process_content(self, content):
        content = re.sub(r" |\r", '', content)
        return content
